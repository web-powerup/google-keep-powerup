import browser from 'webextension-polyfill';
import { openOptionsPage } from './utils/browser';
import { dataStoreListener } from './background/dataStore';
import Logger from './utils/Logger';

Logger.log('Background script is running...');
browser.browserAction.onClicked.addListener(openOptionsPage);
browser.runtime.onMessageExternal.addListener(dataStoreListener);
browser.runtime.onMessage.addListener(dataStoreListener);