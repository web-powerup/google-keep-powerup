'use es6';

import Logger from '../utils/Logger';
const CLASS_NAME_COLLAPSED = 'gkpu-collapsed';

export default class Collapser {
  constructor(options) {
    this.labelsToCollapse = options.confidentials;
    Logger.log('Collapser', this);
  }

  hasToBeCollapsed(note) {
    return this.labelsToCollapse.some(labelName => note.labels && note.labels.includes(labelName));
  }

  isCollapsed(note) {
    return note.elementBody && note.elementBody.classList &&  note.elementBody.classList.contains(CLASS_NAME_COLLAPSED);
  }

  collapse(note) {
    Logger.log('Collapser|collapse', note);
    note.elementBody && note.elementBody.classList && note.elementBody.classList.add(CLASS_NAME_COLLAPSED);
    note.elementCover && note.elementCover.classList && note.elementCover.classList.add(CLASS_NAME_COLLAPSED);
  }

  expand(note) {
    note.elementBody && note.elementBody.classList && note.elementBody.classList.remove(CLASS_NAME_COLLAPSED);
    note.elementCover && note.elementCover.classList && note.elementCover.classList.remove(CLASS_NAME_COLLAPSED);
  }

  apply(note) {
    if (this.hasToBeCollapsed(note)) {
      this.collapse(note);
    } else {
      this.expand(note);
    }
  }
}
