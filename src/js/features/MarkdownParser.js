'use es6';

import hljs from 'highlight.js';
import 'highlight.js/styles/github.css';
import MarkdownIt from 'markdown-it';
import MarkdownItLists from 'markdown-it-task-lists';
import { escapeHtml } from 'markdown-it/lib/common/utils';
import { CLASS_NAME_MARKDOWN } from '../constants/common';
import Logger from '../utils/Logger';

export default class MarkdownParser {
  constructor(options) {
    this.parser = new MarkdownIt({
      highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
          try {
            return '<pre class="hljs"><code>' +
                   hljs.highlight(lang, str, true).value +
                   '</code></pre>';
          } catch (e) {
            Logger.log('MarkdownParser', e);
          }
        }
    
        return '<pre class="hljs"><code>' + escapeHtml(str) + '</code></pre>';
      }
    }).use(MarkdownItLists);
    this.labelsToParseAsMarkdown = options.markdown;
    this.applyAll = options.markdownApplyAll;
    Logger.log('MarkdownParser', this);
  }

  getMarkdownElement(elementBody) {
    return elementBody && elementBody.parentElement && elementBody.parentElement.querySelector(`.${CLASS_NAME_MARKDOWN}`);
  }

  hasToBeParsed(note) {
    return this.applyAll && !note.isList() || this.labelsToParseAsMarkdown.some(labelName => note.labels && note.labels.includes(labelName));
  }

  isRendered(note) {
    note.elementBody.classList.contains(CLASS_NAME_MARKDOWN);
  }

  renderMarkdown(elementBody, text) {
    Logger.log('Markdown|renderMarkdown', elementBody);
    if (text) {
      let markdown = this.getMarkdownElement(elementBody);
      if (!markdown) {
        markdown = document.createElement('div');
        markdown.classList.add(CLASS_NAME_MARKDOWN);
        markdown.setAttribute('tabindex',0);
        elementBody.className.split(' ').forEach((className) => (className !== 'IZ65Hb-YPqjbf') ? markdown.classList.add(className) : undefined);
        elementBody.parentElement.insertBefore(markdown, elementBody);
      }
      try {
        const html = this.parser.render(text);
        markdown.innerHTML = html;
      } catch(e) {
        Logger.log('Markdown|ERROR', e);
      }
      return markdown;
    }
  }

  removeMarkdown(elementBody) {
    let markdown = this.getMarkdownElement(elementBody);
    if (markdown) {
      markdown.parentElement.removeChild(markdown);
    }
  }

  apply(note) {
    if (this.hasToBeParsed(note)) {
      this.renderMarkdown(note.elementBody, note.text);
    } else {
      this.removeMarkdown(note.elementBody);
    }
  }

  applyOnExpandedNote(elementBody, note) {
    if (elementBody) {
      if (this.hasToBeParsed(note)) {
        let markdown = this.renderMarkdown(elementBody, note.text);
        const handler = () => {
          markdown.remove();
          elementBody.addEventListener('focusout', () => {
            note.text = elementBody.innerHTML.replace(/<br\s*\/?>/gi,'\n');
            this.applyOnExpandedNote(elementBody, note);
          });
          elementBody.focus();
          elementBody.parentElement.scroll(0, 0);
        };
        markdown && markdown.addEventListener('click', handler);
        markdown && markdown.addEventListener('focus', handler); 
      } else {
        this.removeMarkdown(elementBody);
      }
    }
  }
}
