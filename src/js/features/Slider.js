'use es6';

import Logger from '../utils/Logger';
import { CLASS_NAME_ARROW, CLASS_NAME_LEFT, CLASS_NAME_RIGHT } from '../constants/common';
import { getComputedStylePropertyValue } from '../utils/dom';

export default class Slider {

  createArrow(className) {
    const arrowContainer = document.createElement('div');
    arrowContainer.classList.add(CLASS_NAME_ARROW);
    arrowContainer.classList.add(className);
    const arrow = document.createElement('div');
    arrow.classList.add(className);
    arrowContainer.appendChild(arrow);
    return arrowContainer;
    
  }

  getLeftArrow(serverId) {
    const arrow = this.createArrow(CLASS_NAME_LEFT);
    arrow.addEventListener('click', function(event){
      event.preventDefault();
      event.stopPropagation();
      window.location.replace(`#NOTE/${serverId}`);
    });
    return arrow;
  }

  getRightArrow(serverId) {
    const arrow = this.createArrow(CLASS_NAME_RIGHT);
    arrow.addEventListener('click', function(event){
      event.preventDefault();
      event.stopPropagation();
      window.location.replace(`#NOTE/${serverId}`);
    });
    return arrow;
  }

  apply(location, nodes) {
    const potentialBackgrounds = Array.from(document.querySelectorAll('div[aria-hidden="true"][style^="width"]')).filter((element) => {
      return getComputedStylePropertyValue(element, 'z-index') === 4000; 
    });
    if (potentialBackgrounds.length) {
      const background = potentialBackgrounds[0];
      background.innerHTML = '';
      Logger.log('Iterator|apply', background, location );
      const index = nodes.findIndex((node) => {
        return node.serverId === location.getServerId();
      });
      if (index > -1) {
        const next = nodes[index === nodes.length - 1 ? 0 : index + 1];
        background.appendChild(this.getRightArrow(next.serverId));
        const prev = nodes[index === 0 ? nodes.length - 1 : index - 1];
        background.appendChild(this.getLeftArrow(prev.serverId));
      }
    }

  }
}
