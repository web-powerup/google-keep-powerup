
'use es6';

import css from 'css';
import 'highlight.js/styles/github.css';
import { CLASS_NAME_MARKDOWN } from '../constants/common';
import { appendStyleNode } from '../utils/dom';
import Logger from '../utils/Logger';

function prefix(rule) {
  if (rule.selectors) {
    rule.selectors = rule.selectors.map( selector => {
      if (selector.indexOf('html') === 0) {
        return selector.replace('html', `div.${CLASS_NAME_MARKDOWN}`);
      } else if (selector.indexOf('body') === 0) {
        return selector.replace('body', `div.${CLASS_NAME_MARKDOWN}`);
      }
      return `div.${CLASS_NAME_MARKDOWN} ${selector}`;
    });
  } else if(rule.rules) {
    parseRules(rule.rules);
  }
}

function parseRules(rules) {
  rules.forEach(prefix);
}

export default function injectMarkdownStyle(options) {
  const { markdownStyle } = options;
  if (options.markdownStyle && options.markdownStyle.value) {
    Logger.log('MarkdownStyler|injectMarkdownStyle', markdownStyle);
    try {
      const obj = css.parse(markdownStyle.value);
      parseRules(obj.stylesheet.rules);
      appendStyleNode(css.stringify(obj));
    } catch(e) {
      Logger.error('MarkdownStyler|injectMarkdownStyle', e);
    }
  }
}
