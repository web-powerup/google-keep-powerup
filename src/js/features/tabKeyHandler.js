'use es6';

function isTextNode(node) {
  return  (Node.TEXT_NODE === node.nodeType);
}

function textNodesUnder(el){
  let n, a=[], walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
  while(n = walk.nextNode()) a.push(n); // eslint-disable-line no-cond-assign
  return a;
}

export default (event) => {
  if (event.keyCode === 9) {
    event.preventDefault();
    const selection = window.getSelection();
    if (selection.isCollapsed) {
      if (event.shiftKey) {
        document.execCommand('delete');
      } else {
        document.execCommand('insertText', false, '\t');
      }
    } else {
      const range = selection.getRangeAt(0);
      const container = range.commonAncestorContainer;
      if (container.nodeValue) {
        const text = container.nodeValue || container.innerText;
        const beforeSelectedText = text.substring(0, range.startOffset);
        const selectedText = text.substring(range.startOffset, range.endOffset);
        const afterSelectedText = text.substring(range.endOffset, text.length);
        if (event.shiftKey) {
          container.nodeValue = beforeSelectedText + (selectedText[0] === '\t') ? selectedText : selectedText + afterSelectedText;
        } else {
          container.nodeValue = beforeSelectedText + '\t' + selectedText + afterSelectedText;
        }
      } else {
        let hasToAddTab = false;
        const nodes = textNodesUnder(container);
        const startContainer = range.startContainer|| range.endContainer;
        const endContainer = range.endContainer.nodeValue === null ? range.startContainer : range.endContainer;

        nodes.filter(isTextNode).forEach((node) => {
          if (node.isEqualNode(startContainer)) {
            hasToAddTab = true;
          }
          if (hasToAddTab) {
            if (event.shiftKey) {
              node.nodeValue = (node.nodeValue[0] === '\t') ? node.nodeValue.substring(1) : node.nodeValue;
            } else {
              node.nodeValue = '\t' + node.nodeValue;
            }

          }
          if (node.isEqualNode(endContainer)) {
            hasToAddTab = false;
          }
        });
      }
    }
  }
};
