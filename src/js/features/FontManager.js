'use es6';

import Logger from '../utils/Logger';
import { appendStyleNode } from '../utils/dom';
import { CUSTOM_FONT_CLASSNAME_PREFIX } from '../constants/common';
import fontFamilies from '../constants/fontFamilies';

function formatFontFamilyName(fontFamilyName) {
  return fontFamilyName && fontFamilyName.replace(/ /g, '+');
}

function formatFontFamilyClassName(label) {
  if (label) {
    return `${CUSTOM_FONT_CLASSNAME_PREFIX}${label.split(' ').join('-').toLowerCase().replace(/ /g, '')}`;
  }
  return label;
}

export default class FontManager {
  constructor(options) {
    this.colors = options && options.customFonts.colors;
    this.fonts = options && options.customFonts.fonts;
    this.labels = options && options.customFonts.labels;
    this.sizes = options && options.customFonts.sizes;
    this.importedFontFamilies = [];
    this.injectedFontStyles = [];
    Logger.log('FontManager', this);
  }
  
  importFontFamily(fontFamilyName) {
    if (!this.importedFontFamilies.includes(fontFamilyName)) {
      Logger.log('FontManager|importFontFamily', fontFamilyName);
      this.importedFontFamilies.push(fontFamilyName);
      const link = document.createElement('link');
      link.href = `https://fonts.googleapis.com/css?family=${formatFontFamilyName(fontFamilyName)}`;
      link.rel = 'stylesheet';
      document.head.appendChild(link);
    }
  }

  injectFontFamilyStyle(color, fontFamilyName, label, size) {
    const className = formatFontFamilyClassName(label);
    if (!this.injectedFontStyles.includes(className)) {
      Logger.log('FontManager|injectFontFamilyStyle', fontFamilyName);
      this.injectedFontStyles.push(className);
      const fontGenericFamily = fontFamilies[fontFamilyName];
      const fontSize = `${size}rem`;
      const lineHeight = `${size*1.05}rem`;
      appendStyleNode(
        `div.${className} div[aria-multiline="true"][role="textbox"]~div[aria-multiline="true"][role="textbox"],
div.${className} div[aria-multiline="true"][role="textbox"]~div div[aria-multiline="true"],
div.${className} .gkpu-markdown * {
  color:${color};
  font-family:'${fontFamilyName}', ${fontGenericFamily};
}
div.${className} div[aria-multiline="true"][role="textbox"]~div[aria-multiline="true"][role="textbox"],
div.${className} div[aria-multiline="true"][role="textbox"]~div div[aria-multiline="true"] {
  font-size:${fontSize};
  line-height:${lineHeight};
}`
      );
    }
  }

  getFirstCustomFontOptions(note) {
    const labels = note.labels || [];
    return this.labels.reduce((hasFound, label, index) => {
      const color = this.colors[index] || '#000';
      const font = this.fonts[index];
      const size = this.sizes[index] || 1.125;
      return hasFound || labels.indexOf(label) > -1 && { color, font, label, size };
    }, false);
  }

  removeAllFontClassNames(elementContainer) {
    Logger.log('FontManager|removeAllFontClassNames', elementContainer);
    elementContainer.classList.forEach((className) => {
      if (className.indexOf(CUSTOM_FONT_CLASSNAME_PREFIX) > -1) {
        elementContainer.classList.remove(className);
      }
    });
  }

  apply(note) {
    const { color, font, label, size } = this.getFirstCustomFontOptions(note);
    const elementContainer = note.elementContainer;
    if (label) {
      this.applyCustomFont(color, elementContainer, font, label, size); 
    } else if (elementContainer.className.indexOf(CUSTOM_FONT_CLASSNAME_PREFIX) > -1) {
      this.removeAllFontClassNames(elementContainer);
    }
  }

  applyCustomFont(color, elementContainer, fontFamilyName, label, size) {
    Logger.log('FontManager|applyCustomFont', elementContainer, fontFamilyName);
    this.importFontFamily(fontFamilyName);
    this.injectFontFamilyStyle(color, fontFamilyName, label, size);
    elementContainer.classList.add(formatFontFamilyClassName(label));
  }

  applyOnExpandedNote(elementContainer, note) {
    if (elementContainer) {
      const { label } = this.getFirstCustomFontOptions(note);
      if (label) {
        elementContainer.classList.add(formatFontFamilyClassName(label));
      } else if (elementContainer.className.indexOf(CUSTOM_FONT_CLASSNAME_PREFIX) > -1) {
        this.removeAllFontClassNames(elementContainer);
      }
    }
  }
}
