'use es6';

import MutationSummary from 'mutation-summary';
import browser from 'webextension-polyfill';
import Logger from './utils/Logger';
import { DATA_ATTRIBUTE_UUID, MessagesType } from './constants/common';
import Collapser from './features/Collapser';
import FontManager from './features/FontManager';
import MarkdownParser from './features/MarkdownParser';
import injectMarkdownStyle from './features/markdownStyler';
import Slider from './features/Slider';
import tabKeyHandler from './features/tabKeyHandler';
import Note from './notes/Note';
import { getExpandedNoteElementBody, getExpandedNoteElementTitle, isNewNoteOpen, mapStoreNodeToDom } from './utils/dom';
import { filterStoreNodesByLocation } from './utils/filters';
import Location from './utils/Location';
import OptionsApi from './options/api/optionsApi';

let location = new Location(window.location.hash);
let features;
let store;

new OptionsApi().getOptions()
  .then((options) => {
    Logger.log('content-main|options', options);
    const EXTENSION_ID = browser.runtime.id;

    injectMarkdownStyle(options);
    const slider = new Slider(options);
    features = {
      collapers: new Collapser(options),
      fontManager: new FontManager(options),
      markdownParser: new MarkdownParser(options)
    };

    function handleNotesContainerDomChange(summaries) {
      Logger.log('content-main|handleNodesContainerDomChange', summaries);
      const summary = summaries[0];
      if (summary.removed.length === 2) {
        const id = summary.removed[0].getAttribute(DATA_ATTRIBUTE_UUID);
        if (id) {
          browser.runtime.sendMessage({
            type: MessagesType.NOTE_TRASHED,
            data: {
              id
            }
          });
        }
      }
      if (summary.added.length) {
        renderAll(summary);
      }
    }

    function addObservers(notesContainer) {
      new MutationSummary({
        callback: handleNotesContainerDomChange,
        queries: [{ element: 'div[aria-multiline="true"][contenteditable="false"][role="textbox"]' }],
        rootNode: notesContainer
      });
    }


    function renderAll(reason) {
      Logger.log('content-main|renderAll', reason, location);
      location.replace(window.location.hash);

      if (store) {
        const indexStart = isNewNoteOpen() ? 1 : 0;
        const labels = store.labels;
        const nodes = store.nodes && filterStoreNodesByLocation(location, store);
        nodes.forEach((node, index) => index >= indexStart && renderNodeFromIndex(labels, node, index, indexStart));

        if (location.isExpandedNode() && (reason === 'LOCATION_CHANGED' || location.isFirstLoad())) {
          applyFeaturesOnExpandedNode(nodes);
          applySlider(location, nodes);
        }

        if (location.isLabel()) {
          const token = decodeURIComponent(location.getLabelName());
          document.querySelectorAll('header[role="banner"] li>a>span').forEach((el) => {
            if (el.innerText === token) {
              el.scrollIntoView();
            }
          });
        }

        window.dispatchEvent(new Event('focus'));
      }
    }

    function renderNodeFromIndex(labels, node, index, indexStart = 0) {
      const rootElement = mapStoreNodeToDom(node, index, indexStart);
      if (rootElement) {
        node.gkpuNote = new Note(node, rootElement, labels);
        Logger.log('content-main|renderNoteFromIndex', index, node);
        Object.values(features).forEach(feature => feature.apply(node.gkpuNote));
      }
    }

    function getStoreListener(EXTENSION_ID) {
      return function(message, sender) {
        if (sender.id === EXTENSION_ID) {
          Logger.log('content-main|message', message);
          if (message.type === MessagesType.STORE_UPDATED) {
            store = message.data;
            renderAll(message.type);
          }
        }
      };
    }

    function getExpandedNode(nodes) {
      const serverId = location.hash.substr(6);
      const findByServerId = node => node.serverId === serverId;
      return nodes && nodes.find(findByServerId);
    }

    function applyFeaturesOnExpandedNode(nodes) {
      const node = getExpandedNode(nodes);
      const elementBody = getExpandedNoteElementBody();
      const elementTitle =  getExpandedNoteElementTitle();
      if (elementBody && elementTitle && node && node.gkpuNote) {
        elementBody.onkeydown = tabKeyHandler;
        features.fontManager.applyOnExpandedNote(elementTitle && elementTitle.parentElement, node.gkpuNote);
        features.markdownParser.applyOnExpandedNote(elementBody, node.gkpuNote);
      }
    }

    function applySlider(location, nodes) {
      if (nodes && nodes.length) {
        slider.apply(location, nodes);
      }
    }

    const bootstrap = setInterval(() => {
      const notesContainer = document.querySelector('.notes-container');
      if (notesContainer) {
        clearInterval(bootstrap);
        addObservers(notesContainer);
        Logger.log('content-main|observer initilized');
      }
    }, 25);

    setInterval(() => {
      if (location.hash !== window.location.hash) {
        renderAll('LOCATION_CHANGED');
      }
    }, 100);

    browser.runtime.onMessage.addListener(getStoreListener(EXTENSION_ID));
  });
