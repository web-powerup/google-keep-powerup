'use es6';

import browser from 'webextension-polyfill';
import { MessagesType } from './constants/common';

(function(doc){
  const EXTENSION_ID = browser.runtime.id;

  if (doc.location.pathname === '/static/proxy.html') {
    (function(){
      var s = doc.createElement('script');
      s.type = 'text/javascript';
      s.nonce = '';
      (doc.head || doc.documentElement).appendChild(s);
      s.text =`
XMLHttpRequest.prototype.open = (function(orig){
  return function() {
    this.addEventListener("load", function() {
      try {
        if (this.responseText && this.responseText.length > 0) {
          const response = JSON.parse(this.responseText);
          const message = {
            data: {},
            type: "${MessagesType.CHANGE_PULL}"
          }
          if (response && response.nodes) {
            message.data.nodes = response.nodes;
          }
          if (response && response.userInfo && response.userInfo.labels) {
            message.data.labels = response.userInfo.labels;
          }
          if (message.data.labels && message.data.labels.length || message.data.nodes && message.data.nodes.length) {
            chrome.runtime.sendMessage("${EXTENSION_ID}", message);
          }
        }
      } catch(error) {
      }
    });
    return orig.apply(this, arguments);
  }
})(XMLHttpRequest.prototype.open);
      `;
    })();
  }

})(document);