'use es6';
import browser from 'webextension-polyfill';
import { MessagesType } from '../constants/common';
import { isParentRoot } from '../utils/keepNode';
import Logger from '../utils/Logger';

const store = {
  labels: [],
  nodes: []
};

function extractAndComposeStoreNodes(keepNodes) {
  const nodes = [];
  const lookupTable = {};
  for (let i = 0; i < keepNodes.length; i++) {
    const keepNode = keepNodes[i];
    if (isParentRoot(keepNode)) {
      nodes.push(keepNode);
      lookupTable[keepNode.id] = keepNode;
    }
  }
  for (let i = 0; i < keepNodes.length; i++) {
    const keepNode = keepNodes[i];
    if (!isParentRoot(keepNode)) {
      const parentNode = lookupTable[keepNode.parentId];
      if (keepNode.text) {
        parentNode.text = keepNode.text;
      }
    }
  }
  return nodes;
}

function findNodeByMatchingProperties(nodes, node, nodeMatchingProperty, matchingPropertyNodes, searchMethod = 'find') {
  return nodes[searchMethod](current => current[matchingPropertyNodes] === node[nodeMatchingProperty]);
}

function applyChange(nodes, node) {
  if (nodes.length) {
    const index = findNodeByMatchingProperties(nodes, node, 'id', 'id', 'findIndex');
    let nodeToChange = nodes[index];
    if (nodeToChange) {
      let lastUpdateNodeChanged = new Date(node.timestamps.updated).getTime();
      let lastUpdateNodeToChange = new Date(nodeToChange.timestamps.updated).getTime();
      if ( lastUpdateNodeChanged >= lastUpdateNodeToChange) {
        if (!node.text && nodeToChange.text) {
          node.text = nodeToChange.text;
        }
        nodes[index] = node;
      }
    } else {
      nodes.push(node);
    }
  }
}

function sendUpdate() {
  return browser.tabs.query({url: 'https://keep.google.com/*'})
    .then(tabs => {
      return Promise.all(
        Array.from(tabs, (tab) => {
          Logger.log(`dataStoreListener|sending message to ${tab.id}`, store);
          return browser.tabs.sendMessage(tab.id, {
            data: store,
            type: MessagesType.STORE_UPDATED
          }).catch(error => {
            Logger.log('dataStoreListener|ERROR|', error && error.message);
            setTimeout(sendUpdate, 250);
          });
        })
      );
    });
}

export function dataStoreListener(message, sender) {
  Logger.log('dataStoreListener|recieved message', message);

  if (sender.url.indexOf('https://') === 0 && sender.url.indexOf('.google.com/') >= 0) {
    switch(message.type) {
    case MessagesType.CHANGE_PULL:
      if (message.data.labels) {
        store.labels = message.data.labels;
      }
      if (message.data.nodes) {
        const nodes = extractAndComposeStoreNodes(message.data.nodes);
        nodes.forEach(node => applyChange(store.nodes, node));
      }
      break;
    case MessagesType.LABELS_PRELOADED:
      store.labels = message.data;
      break;
    case MessagesType.NOTE_TRASHED: {
      const now = new Date().toISOString();
      const node = store.nodes.find((node) => node.id === message.data.id);
      node.timestamps.trashed = now;
      node.timestamps.updated = now;
      break;
    }
    case MessagesType.NOTES_PRELOADED:
      store.nodes = extractAndComposeStoreNodes(message.data);
      break;
    }

    switch(message.type) {
    case MessagesType.CHANGE_PULL:
    case MessagesType.NOTE_TRASHED:
    case MessagesType.NOTES_PRELOADED:
      sendUpdate();
    }
  }
}
