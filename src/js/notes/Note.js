'use es6';

import { getNoteBodyElementByTitle, getNoteTitleElement } from '../utils/dom';
import { isLabelDeleted } from '../utils/keepNode';


function toLabelName(label) {
  return label && label.name;
}

function isLabelMatchingNodeLabel(label, nodeLabelId) {
  return label.mainId === nodeLabelId.labelId;
}

function isNodeLabelIdValid(nodeLabelId) {
  return !isLabelDeleted(nodeLabelId);
}

function getNodeLabelIdsToLabelReducer(labels) {
  return (accumulator, nodeLabelId) => {
    const label = labels.find(label => isNodeLabelIdValid(nodeLabelId) && isLabelMatchingNodeLabel(label, nodeLabelId));
    label && accumulator.push(label);
    return accumulator;
  };
}

function extractLabelNames(nodeLabelsIds,labels) {
  try {
    if (nodeLabelsIds && nodeLabelsIds.length > 0) {
      const nodeLabels = nodeLabelsIds.reduce(getNodeLabelIdsToLabelReducer(labels), []);
      const nodeLabelNames = nodeLabels.map(toLabelName);
      return nodeLabelNames;
    }
  } catch (e) {
    console.error('Note|extractLabelNames', e);
  }
}

export default class Note {
  constructor(node, rootElement, labels) {
    this.elementContainer = rootElement;
    this.text = node.text;
    this.title = node.title;
    this.type = node.type;
    this.elementContainer.classList.remove('gkpu-container');
    this.elementContainer.classList.add('gkpu-container');
    this.elementTitle = getNoteTitleElement(rootElement);
    this.elementBody = this.isList() ? this.elementTitle.nextElementSibling : getNoteBodyElementByTitle(this.elementTitle);
    this.elementCover = (this.elementTitle.previousSibling && this.elementTitle.previousSibling.innerHTML.indexOf('<img') ? this.elementTitle.previousSibling : null);
    this.labels = extractLabelNames(node.labelIds, labels);
  }

  isList() {
    return this.type === 'LIST';
  }

  isNote() {
    return this.type === 'NOTE';
  }
}
