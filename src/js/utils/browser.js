'use es6';

import browser from 'webextension-polyfill';

export function openOptionsPage() {
  const url = browser.extension.getURL('/html/options.html'); 
  openUrlOrTab(url, true);
}

export function openUrlOrTab(url, selected) {
  browser.tabs.query({url: url})
    .then(tabs => {
      if (tabs.length > 0) {
        return browser.tabs.update(tabs[0].id, { selected });
      }
      return browser.tabs.create({url: url});
    });
}