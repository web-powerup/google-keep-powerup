'use es6';

import { BEGINNING_OF_TIME, Kinds } from '../constants/common';
import { negate } from './common';

function isNode(node) {
  return node && node.kind === Kinds.NODE;
}

export function compareBySortValue(node1, node2) {
  const node1SortValue = parseInt(node1.sortValue);
  const node2SortValue = parseInt(node2.sortValue);
  return node2SortValue - node1SortValue;
}

export function compareByUpdatedTimestamp(node1, node2) {
  return compareByTimestampDescending('updated', node1, node2);
}

export function compareByUserEditedTimestamp(node1, node2) {
  return compareByTimestampDescending('userEdited', node1, node2);
}

function compareByTimestampDescending(primaryType, node1, node2) {
  const node1Timestamp = Date.parse(node1.timestamps[primaryType]);
  const node2Timestamp = Date.parse(node2.timestamps[primaryType]);
  return node2Timestamp - node1Timestamp;
}

export function hasColor(node, colorName) {
  return node.color && node.color.toLowerCase() === colorName;
}

export function hasLabel(node, label) {
  return label && node.labelIds && node.labelIds.find && node.labelIds.find(nodeLabel => nodeLabel.labelId === label.mainId);
}

export function isLabelDeleted(nodeLabelId) {
  return nodeLabelId.deleted && nodeLabelId.deleted !== BEGINNING_OF_TIME;
}

export function isArchived(node) {
  return isNode(node) && node.isArchived;
}

export function isNotArchived(node) {
  return negate(isArchived)(node);
}

export function isPinned(node) {
  return isNode(node) && node.isPinned;
}

export function isNotPinned(node) {
  return negate(isPinned)(node);
}


export function isParentRoot(node) {
  return isNode(node) && node.parentId === 'root';
}

export function isTrashed(node) {
  return isNode(node) && node.timestamps && node.timestamps.trashed && node.timestamps.trashed !== BEGINNING_OF_TIME;
}

export function isNotTrashed(node) {
  return negate(isTrashed)(node);
}

export function isOthers(node) {
  return isNotArchived(node) && isNotTrashed(node) && isNotPinned(node);
}

