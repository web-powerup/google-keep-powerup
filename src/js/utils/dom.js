'use es6';

import { negate }  from './common';
import { DATA_ATTRIBUTE_UUID } from '../constants/common';

export function extractLabels(root) {
  const labels = Array.from(root.querySelectorAll('label')).filter(isNotEmpty).map(getText);
  return labels;
}

export function getComputedStylePropertyValue(element, property) {
  return element.computedStyleMap().get(property).value;
}

export function getElementsByXPath(xpath, parent)
{
  let results = [];
  let query = document.evaluate(xpath,
    parent || document,
    null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
  for (let i=0, length=query.snapshotLength; i<length; ++i) {
    results.push(query.snapshotItem(i));
  }
  return results;
}

export function getExpandedNoteElementTitle() {
  return document.querySelector('div[aria-hidden="true"][style^="width"] ~ div[style^="left"] div[aria-multiline="true"][role="textbox"]');
}

export function getExpandedNoteElementBody() {
  return document.querySelectorAll('div[aria-hidden="true"][style^="width"] ~ div[style^="left"] div[aria-multiline="true"][role="textbox"]')[1];
}

export function getExpandedNoteElements() {
  return document.querySelectorAll('div.notes-container ~ div div[aria-multiline="true"][contenteditable="true"][role="textbox"]');
}

export function getNewNoteElements() {
  return document.querySelectorAll('div.notes-container > div div[style^="width"] div[aria-multiline="true"][contenteditable="true"][role="textbox"]');
}

export function getNoteBodyElementByTitle(titleElement) {
  return getElementsByXPath('..//..//div[@contenteditable="false"][@aria-multiline="true"][@role="textbox"]', titleElement)[1];
}

export function getNoteTitleElement(rootElement) {
  return getElementsByXPath('.//div[@contenteditable="false"][@aria-multiline="true"][@role="textbox"]', rootElement)[0];
}

export function getNoteRootElementByIndex(index) {
  return getElementsByXPath('/html/body//*/div/div[starts-with(@style,"height")]/div/div[@role="button"]/..')[index];
}

export function getNoteRootElementByUuid(id) {
  return document.querySelector(`div[${DATA_ATTRIBUTE_UUID}="${id}"]`);
}

export function getText(element) {
  return element && element.innerText;
}

export function isEmpty(element) {
  return !getText(element) || getText(element).length < 1;
}

const isNotEmpty = negate(isEmpty);

export function isHidden(element) {
  if (element) {
    var style = window.getComputedStyle(element);
    return (style.display === 'none');
  }
  return undefined;
}

export function isNewNoteOpen() {
  const newNoteElements = getNewNoteElements();
  return newNoteElements && newNoteElements.length && !isHidden(newNoteElements[0]) && !isHidden(newNoteElements[1]);
}

export function mapStoreNodeToDom(node, index, indexStart = 0) {
  const noteRootElement = getNoteRootElementByUuid(node.id) || getNoteRootElementByIndex(index - indexStart);
  if (noteRootElement) {
    const setId = (element) => element.setAttribute(DATA_ATTRIBUTE_UUID, node.id);
    setId(noteRootElement);
  }
  return noteRootElement;
}

export function appendStyleNode(styleBody) {
  const style = document.createElement('style');
  style.type = 'text/css';
  style.innerHTML = styleBody;
  document.head.appendChild(style);
}