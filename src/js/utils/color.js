'use es6';
function componentToHex(c) {
  var hex = c.toString(16).toUpperCase();
  return hex.length === 1 ? `0${hex}` : hex;
}

function rgbToHex(r, g, b) {
  return `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
}

function cssColor2ColorAlpha(css) {
  if (css.indexOf('rgba') === 0) {
    const [r, g, b, a] = css.substring(5, css.length - 1).split(',').map((n,i) => i<3 ? parseInt(n, 10) : parseFloat(n, 10));
    return {
      alpha: a * 100,
      color: rgbToHex(r,g,b)
    };
  }
  return {
    alpha: 100,
    color: css.toUpperCase()
  };
}

function hex2rgba(hex, alpha = 100) {
  const [r, g, b] = hex.match(/\w\w/g).map(x => parseInt(x, 16));
  return `rgba(${r},${g},${b},${alpha / 100})`;
}

export {
  cssColor2ColorAlpha,
  hex2rgba
};