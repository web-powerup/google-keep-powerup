'use es6';

import { Locations } from '../constants/common';
import { compareBySortValue, compareByUserEditedTimestamp, hasColor, hasLabel, isArchived, isNotArchived, isNotTrashed } from './keepNode';

const filterOutArchivedAndTrashedNotes = (node) => isNotArchived(node) && isNotTrashed(node);

const filterOutNotArchivedNotes = (node) => isArchived(node);

const getFilterByColorName = (colorName) => (node) => isNotTrashed(node) && hasColor(node, colorName);

const getFilterByLabel = (label) => (node) => isNotTrashed(node) && hasLabel(node,label);

export function findLabelByName(labels, name) {
  return labels.find(label => label.name === name);
}

function getFilterByLocation(location, labels) {
  let colorName, labelName;
  switch(location.route) {
  case '':
  case Locations.ARCHIVE: {
    return filterOutNotArchivedNotes;
  }
  case Locations.HOME: {
    return filterOutArchivedAndTrashedNotes;
  }
  case Locations.LABEL: {
    labelName = location.getLabelName();
    const label = findLabelByName(labels, labelName);
    return getFilterByLabel(label);
  }
  case Locations.LIST:
  case Locations.NOTE: {
    if (location.previous) {
      return getFilterByLocation(location.previous, labels);
    }
    break;
  }
  case Locations.SEARCH:{
    colorName = location.getColorName();
    labelName = location.getLabelName();
    break;
  }
  }

  if (colorName) {
    return getFilterByColorName(colorName);
  }

  if (labelName) {
    const label = findLabelByName(labels, labelName);
    return getFilterByLabel(label);
  }

  return isNotTrashed;
}

function pinnedAndOthersBySortValueComparator(node1, node2) {
  if (node1.isPinned === node2.isPinned) {
    return compareBySortValue(node1, node2);
  }
  return node1.isPinned ? -1 : 1;
}

function pinnedOthersArchivedBySortValueComparator(node1, node2) {
  if (node1.isArchived && node2.isArchived ||
       node1.isPinned && node2.isPinned ||
      !node1.isArchived  && !node1.isPinned && !node2.isArchived && !node2.isPinned
  ) {
    return compareBySortValue(node1, node2);
  }
  return node1.isPinned || node2.isArchived ? -1 : 1;
}

function othersArchivedByUserEditedComparator(node1, node2) {
  if (node1.isArchived === node2.isArchived) {
    return compareByUserEditedTimestamp(node1, node2);
  }
  return node2.isArchived ? -1 : 1;
}

function getSorterByLocation(location) {
  switch(location.route) {
  case '':
  case Locations.HOME: {
    return pinnedAndOthersBySortValueComparator;
  }
  case Locations.LABEL: {
    return pinnedOthersArchivedBySortValueComparator;
  }
  case Locations.LIST:
  case Locations.NOTE: {
    if (location.previous) {
      return getSorterByLocation(location.previous);
    }
    break;
  }
  case Locations.SEARCH:{
    return othersArchivedByUserEditedComparator;
  }
  }
  return pinnedAndOthersBySortValueComparator;
}

export function filterStoreNodesByLocation(location, store) {
  return store.nodes.filter(getFilterByLocation(location, store.labels)).sort(getSorterByLocation(location)) || [];
}
