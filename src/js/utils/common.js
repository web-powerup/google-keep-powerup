'use es6';

function copyJSON(src) {
  return JSON.parse(JSON.stringify(src));
}


function debounce(func, ms = 0) {
  let timer = null;
  return (...args) => new Promise(resolve => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      return resolve(func(...args));
    }, ms);
  });
}

function negate(func) {
  return function () {
    return !func.apply(this, arguments);
  };
}

export { copyJSON, debounce, negate };
