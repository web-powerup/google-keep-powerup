'use es6';

import { Locations } from '../constants/common';

const REGEX_SEARCH_COLOR = new RegExp(`${Locations.SEARCH}/color[=/]`);
const REGEX_SEARCH_TAGS = new RegExp(`${Locations.SEARCH}/tags[=/]`);
const TEST_SEARCH_COLOR = new RegExp(`${Locations.SEARCH}/color[=/].*`);
const TEST_SEARCH_TAGS = new RegExp(`${Locations.SEARCH}/tags[=/].*`);

export default class Location {
  constructor(hash) {
    this.setHash(hash);
  }

  getColorName() {
    if (this.isSearch() && TEST_SEARCH_COLOR.test(this.decodedHash)) {
      return this.decodedHash.replace(REGEX_SEARCH_COLOR,'');
    }
  }

  getLabelName() {
    if (this.isLabel()) {
      return this.decodedHash.replace(`${Locations.LABEL}/`,'');
    }
    if (this.isSearch() && TEST_SEARCH_TAGS.test(this.decodedHash)) {
      return this.decodedHash.replace(REGEX_SEARCH_TAGS,'');
    }
  }

  getServerId() {
    if (this.isList()) {
      return this.decodedHash.replace(`${Locations.LIST}/`,'');
    }
    if (this.isNote()) {
      return this.decodedHash.replace(`${Locations.NOTE}/`,'');
    }
  }

  isFirstLoad() {
    return !this.previous;
  }

  isExpandedNode() {
    return this.isList() || this.isNote();
  }

  isHome() {
    return this.isRoute(Locations.HOME) || this.hash === '';
  }

  isLabel() {
    return this.isRoute(Locations.LABEL);
  }

  isList() {
    return this.isRoute(Locations.LIST);
  }

  isNote() {
    return this.isRoute(Locations.NOTE);
  }

  isRoute(routeName) {
    return this.route === routeName;
  }

  isSearch() {
    return this.isRoute(Locations.SEARCH);
  }

  replace(hash) {
    if (this.hash !== hash) {
      if (!this.isNote() && !this.isList()) {
        this.previous = new Location(this.hash);
      }
      this.setHash(hash);
      return true;
    }
    return false;
  }

  setHash(hash) {
    this.hash = hash;
    this.decodedHash = decodeURIComponent(decodeURIComponent(hash));
    const parametersIndex = this.decodedHash.indexOf('/');
    this.route = (parametersIndex > 0) ? this.decodedHash.substring(0, parametersIndex) : this.decodedHash;
  }
}
