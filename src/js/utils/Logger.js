'use es6';

const HEADER = 'Google Keep™ PowerUp';

class Logger {
  constructor() {
    if (!Logger.instance) {
      if (localStorage && localStorage.getItem('DEBUG_KEEP_POWERUP')) {
        Logger.instance = {
          log: console.log.bind(window.console, HEADER),
          error: console.error.bind(window.console, HEADER)
        };
      } else {
        Logger.instance = {
          log: () => {}
        };
      }
    }
    return Logger.instance;
  }
}

// ensure Singleton
const instance = new Logger();
Object.freeze(instance);

export default instance;