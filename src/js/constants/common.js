'use es6';

export const BEGINNING_OF_TIME = '1970-01-01T00:00:00.000Z';
export const CLASS_NAME_MARKDOWN = 'gkpu-markdown';
export const CLASS_NAME_ARROW = 'gkpu-arrow';
export const CLASS_NAME_LEFT = 'gkpu-left';
export const CLASS_NAME_RIGHT = 'gkpu-right';
export const CUSTOM_FONT_CLASSNAME_PREFIX = 'gkpu-ff-';
export const DATA_ATTRIBUTE_UUID = 'data-gkpu-uuid';
export const DEFAULT_OPTIONS = { 
  confidentials: ['confidential'],
  customFonts: {
    colors: ['#000'],
    fonts: ['Patrick Hand SC'],
    labels: ['idea'],
    sizes: [1.125],
  },
  markdown: ['markdown'],
  markdownStyle: { value: '/* Custom CSS for Markdown styling */' }
};
export const FONT_SIZES = [0.75, 1, 1.125, 1.5, 2.0, 2.5, 3.0];
export const Kinds = {
  NODE: 'notes#node',
  TIMESTAMPS: 'notes#timestamps'
};
export const LOCAL_STORAGE_KEY = 'gkpu-local-storage';
export const Locations = {
  ARCHIVE: '#archive',
  HOME: '#home',
  LABEL: '#label',
  LIST: '#LIST',
  NOTE: '#NOTE',
  SEARCH: '#search'
};
export const MessagesType = {
  CHANGE_PULL: 'CHANGE_PULL',
  LABELS_PRELOADED: 'LABELS_PRELOADED',
  NOTE_TRASHED: 'NOTE_TRASHED',
  NOTES_PRELOADED: 'NOTES_PRELOADED',
  STORE_UPDATED: 'STORE_UPDATED',
};
export const NAMESPACE = '79ytcs6g4c7cb885ygzdz089l4ib8ywd';

