'use es6';

import browser from 'webextension-polyfill';
import { MessagesType } from './constants/common';

(function(doc){
  const EXTENSION_ID = browser.runtime.id;
  const script = doc.createElement('script');
  script.type = 'text/javascript';
  script.nonce = '';
  (doc.head || doc.documentElement).appendChild(script);
  script.text =`
(function(w, d){
w.loadChunk = (function(orig){
  return function(data) {
    chrome.runtime.sendMessage("${EXTENSION_ID}", {data: data, type: "${MessagesType.NOTES_PRELOADED}"});
  };
})(w.loadChunk);
w.preloadUserInfo = (function(orig){
  return function(data) {
    chrome.runtime.sendMessage("${EXTENSION_ID}", {data: data.labels, type: "${MessagesType.LABELS_PRELOADED}"});
  };
})(w.preloadUserInfo);
w.preloadUserDasherInfo = (function(orig){
  return function() {};
})(w.preloadUserDasherInfo);
var s = Array.from(d.querySelectorAll('script')).find((script) => { return script.text.indexOf('preloadUserInfo') === 0 });
var sReplay = document.createElement('script');
sReplay.type = 'text/javascript';
(d.head || d.documentElement).appendChild(sReplay);
sReplay.text = s.text;
sReplay.parentNode.removeChild(sReplay);
})(window, document);
  `;
  script.parentNode.removeChild(script);

})(document);