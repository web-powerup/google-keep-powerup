'use es6';

import ActionTypes from '../actions/actionTypes';

export default (currentState, action) => {
  const nextState = new Array(...currentState);
  if (action.type && action.payload) {
    const { index, value } = action.payload;
    switch (action.type) {
    case ActionTypes.COLLAPSER_ADD_LABEL:
      nextState.push(value);
      break;
    case ActionTypes.COLLAPSER_CHANGE_LABEL:
      nextState[index] = value;
      break;
    case ActionTypes.COLLAPSER_REMOVE_LABEL:
      nextState.splice(index, 1);
      break;
    }
  }
  return nextState;
};