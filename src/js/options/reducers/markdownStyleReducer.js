'use es6';

import css from 'css';
import ActionTypes from '../actions/actionTypes';


export default (currentState, action) => {
  let nextState = currentState;
  if (action.type && action.payload) {
    const { value } = action.payload;
    switch (action.type) {
    case ActionTypes.MARKDOWN_CHANGE_STYLE:
      nextState.value = value;
      delete nextState.error;
      try {
        css.parse(value);
      } catch(error) {
        nextState.error = error;
      }
      break;
    }
  }
  return nextState;
};