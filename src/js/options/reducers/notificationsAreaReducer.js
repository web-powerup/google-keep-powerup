'use es6';

import ActionTypes from '../actions/actionTypes';

export default (currentState, action) => {
  const nextState = new Array(...currentState);
  if (action.type && action.payload) {
    const { index } = action.payload;
    switch (action.type) {
    case ActionTypes.NOTIFICATIONAREA_REMOVE_NOTIFICATION:
      nextState.splice(index, 1);
      break;
    }
  }
  return nextState;
};