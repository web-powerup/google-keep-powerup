'use es6';

import ActionTypes from '../actions/actionTypes';
import { DEFAULT_OPTIONS } from '../../constants/common.js';

export default (currentState, action) => {
  const { colors, fonts, labels, sizes } = currentState;
  const nextState = {
    fonts: new Array(...fonts),
    labels: new Array(...labels),
    focusedIndex: currentState.focusedIndex || 0,
    lastIndex: currentState.lastIndex || 0,
  };
  if ( colors ) {
    nextState.colors = new Array(...colors);
  } else {
    nextState.colors = [];
  }
  if ( sizes ) {
    nextState.sizes = [...sizes];
  } else {
    nextState.sizes = [];
  }
  if (action.type && action.payload) {
    const { index, value } = action.payload;
    if (action.type.indexOf('CUSTOM_FONTS') === 0) {
      nextState.focusedIndex = index;
      nextState.lastIndex = index;
      nextState.justAdded = false;
    }
    switch (action.type) {
    case ActionTypes.CUSTOM_FONTS_ADD_LABEL:
      nextState.labels.push(value);
      ['colors', 'fonts', 'sizes'].forEach(property => nextState[property].push(DEFAULT_OPTIONS.customFonts[property][0]));
      nextState.justAdded = true;
      break;
    case ActionTypes.CUSTOM_FONTS_CHANGE_COLOR:
      nextState.colors[index] = value;
      break;
    case ActionTypes.CUSTOM_FONTS_CHANGE_FOCUS:
      nextState.focusedIndex = index;
      break;
    case ActionTypes.CUSTOM_FONTS_CHANGE_FONT:
      nextState.fonts[index] = value;
      break;
    case ActionTypes.CUSTOM_FONTS_CHANGE_LABEL:
      nextState.labels[index] = value;
      break;
    case ActionTypes.CUSTOM_FONTS_CHANGE_SIZE:
      nextState.sizes[index] = value;
      break;
    case ActionTypes.CUSTOM_FONTS_REMOVE_LABEL:
      ['colors', 'fonts', 'labels', 'sizes'].forEach(property => nextState[property].splice(index, 1));
      if (nextState.lastIndex === index) {
        nextState.lastIndex = index ? index - 1 : 0;
      }
      if (nextState.focusedIndex === index) {
        nextState.focusedIndex = index ? index - 1 : 0;
      }
      break;
    }
  }
  return nextState;
};