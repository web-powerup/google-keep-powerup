'use es6';

import ActionTypes from '../actions/actionTypes';
import confidentialNotesReducer from './confidentialNotesReducer';
import customFontsReducer from './customFontsReducer';
import markdownNotesReducer from './markdownNotesReducer';
import markdownStyleReducer from './markdownStyleReducer';
import notificationsAreaReducer from './notificationsAreaReducer';

const markdownApplyAllReducer  = (currentState, action) => {
  if (action.type && action.payload) {
    const value = action.payload.value;
    switch (action.type) {
    case ActionTypes.MARKDOWN_TOGGLE_APPLY_ALL:
      return value;
    }
  }
  return currentState;
};

export default (state, action) => {
  const nextState = {
    options: {
      customFonts: customFontsReducer(state.options.customFonts, action),
      confidentials: confidentialNotesReducer(state.options.confidentials, action),
      markdown: markdownNotesReducer(state.options.markdown, action),
      markdownApplyAll: markdownApplyAllReducer(state.options.markdownApplyAll, action),
      markdownStyle: markdownStyleReducer(state.options.markdownStyle, action),
    },
    notifications: notificationsAreaReducer(state.notifications || [], action),
    saved: action.type === ActionTypes.OPTIONS_SAVED,
  };
  if (nextState.saved) {
    nextState.notifications = nextState.notifications || [];
    nextState.notifications.push({ 
      message: 'SAVED! Please remember to refresh keep.google.com.',
      state: 'success'
    });
  }
  nextState.notificationsChanged = (state.notifications && nextState.notifications && state.notifications.length !== nextState.notifications.length);
  return nextState;
};
