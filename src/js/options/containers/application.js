'use es6';
import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  AppBar,
  Card,
  CardHeader,
  CardMedia,
  Container,
  Grid,
  SnackbarContent,
  Toolbar,
  Typography
} from '@material-ui/core';
import { amber, green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import InfoIcon from '@material-ui/icons/Info';
import ConfidentialNotes from '../components/confidentialNotes';
import CustomFonts from '../components/customFonts';
import CustomFontsPreview from '../components/customFontsPreview';
import Markdown from '../components/markdown';
import MarkdownStyle from '../components/markdownStyle';
import MarkdownCodeSnippetInstructions from '../components/markdownCodeSnippetInstructions';
import NotificationsArea from '../components/notificationsArea';

const variantIcon = {
  info: InfoIcon,
};

const styles = makeStyles(theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.secondary.main,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  margin: {
    marginBottom: theme.spacing(2),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}));

function SnackbarContentWrapper(props) {
  const classes = styles();
  const { className, message, variant } = props;
  const Icon = variantIcon[variant];


  return (
    <SnackbarContent
      className={clsx(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
    />
  );
}
SnackbarContentWrapper.propTypes = {
  className: PropTypes.string,
  message: PropTypes.node,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

export default function render({ version }) {
  const classes = styles();
  return (
    <React.Fragment>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h2">
            <strong>Google</strong> Keep PowerUp - {version}
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="xl" style={{marginTop: '96px'}}>
        <SnackbarContentWrapper
          variant="info"
          className={classes.margin}
          message={<Typography>Google Keep PowerUp - {version} adds check lists for Markdown. Edit your markdown notes using the <strong>- [ ]</strong> and <strong>- [x]</strong> syntax to create your check list.</Typography>}
        />
        <Grid container={true} spacing={3}>
          <Grid item={true} xs={8}>
            <ConfidentialNotes />
          </Grid>
          <Grid item={true} xs={4}>
            <Card style={{height:'100%'}}>
              <CardHeader title="Latest Demo" />
              <CardMedia
                component="iframe"
                src="https://www.youtube.com/embed/wb7zhRLfJuE"
                style={{border:'none',height:'100%'}}
              />
            </Card>
          </Grid>
          <Grid item={true} xs={8}>
            <CustomFonts />
          </Grid>
          <Grid item={true} xs={4}>
            <CustomFontsPreview />
          </Grid>
          <Grid item={true} xs={4}>
            <Markdown />
          </Grid>
          <Grid item={true} xs={4}>
            <MarkdownStyle />
          </Grid>
          <Grid item={true} xs={4}>
            <MarkdownCodeSnippetInstructions />
          </Grid>
        </Grid>
      </Container>
      <NotificationsArea />
    </React.Fragment>
  );
}

render.propTypes = {
  version: PropTypes.string.isRequired,
};
