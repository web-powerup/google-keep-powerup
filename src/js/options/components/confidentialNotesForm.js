'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  FormLabel,
  Grid,
} from '@material-ui/core';

import LabelInput from './inputs/labelInput';
import { addCollapserLabel, changeCollapserLabel, removeCollapserLabel } from '../actions';

class ConfidentialNotesForm extends Component {

  renderLabelInputs({ addCollapserLabel, changeCollapserLabel, labels, removeCollapserLabel }) {
    const elements = labels.map((label, index) => {
      return (
        <LabelInput
          handleOnChange={changeCollapserLabel}
          handleOnDelete={removeCollapserLabel}
          index={index}
          name={`labels.${index}`}
          key={`label${index}`}
          sizes={[5,5]}
          value={label}
        />
      );
    });
    elements.push((
      <LabelInput
        handleOnChange={addCollapserLabel}
        index={elements.length}
        label="Type an existing label name" 
        name="addNewLabel"
        key={`label${elements.length}`}
        sizes={[5,5]}
        value=""
      />
    ));
    return elements;
  }

  render() {
    if (!this.props.labels) {
      return 'Loading';
    }
    return (
      <form className="collapser">
        <Grid item={true} style={{marginTop: '3em'}} xs={4}>
          <FormLabel>Label</FormLabel>
        </Grid>
        {this.renderLabelInputs(this.props)}
      </form>
    );
  }
}

ConfidentialNotesForm.propTypes = {
  labels: PropTypes.array
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addCollapserLabel,
      changeCollapserLabel,
      removeCollapserLabel,
    },
    dispatch
  );
};

const mapStateToProps = ({ options: { confidentials: labels} }) => {
  return {
    labels,
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(ConfidentialNotesForm);
