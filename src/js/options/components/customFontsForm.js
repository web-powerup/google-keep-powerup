/* eslint-disable */
'use es6';

import 'rc-color-picker/assets/index.css';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ColorPicker from 'rc-color-picker';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/NativeSelect';
import LabelInput from './inputs/labelInput';
import { addCustomFontsLabel, changeCustomFontsProperty, removeCustomFontsLabel } from '../actions';
import fontFamilies from '../../constants/fontFamilies';
import { FONT_SIZES } from '../../constants/common';
import { cssColor2ColorAlpha, hex2rgba } from '../../utils/color';

class CustomFontsForm extends Component {

  renderLabelInputs({ addCustomFontsLabel, changeCustomFontsProperty, customFonts, removeCustomFontsLabel }) {
    const elements = [];
    if (customFonts.labels && customFonts.labels.length) {
      elements.push(
        <Grid
          alignItems="center"
          className="container"
          container={true}
          direction="row"
          key="header"
          spacing={2}
          style={{marginTop: '3em'}} 
        >     
          <Grid item={true} xs={4}>
            <FormLabel>Label</FormLabel>
          </Grid>
          <Grid item={true} xs={4}>
            <FormLabel>Font</FormLabel>
          </Grid>
          <Grid item={true} xs={2}>
            <FormLabel>Size</FormLabel>
          </Grid>
          <Grid item={true} xs={1}>
            <FormLabel>Color</FormLabel>
          </Grid>
          <Grid item={true} xs={1}>
            <FormLabel></FormLabel>
          </Grid>
        </Grid>
      );
      elements.push(customFonts.labels.map((label, index) => {
        const { alpha, color } = cssColor2ColorAlpha(customFonts.colors[index]);
        const focus = customFonts.focusedIndex === index && customFonts.justAdded;
        const font = customFonts.fonts[index];
        const size = customFonts.sizes[index];
        return (
          <LabelInput
            handleOnChange={(index, value) => changeCustomFontsProperty('label', index, value)}
            handleOnDelete={removeCustomFontsLabel}
            handleOnFocus={ () => changeCustomFontsProperty('focus', index) }
            focus={focus}
            index={index}
            name={`row-${index}`}
            sizes={[4,2,1]}
            key={`row-${index}`}
            value={label}
          >
            <FormControl style={{width: '100%'}}>
              <Select
                id={`font-${index}`}
                onChange={e => changeCustomFontsProperty('font', index, e.target.value)}
                value={font}
              >
                {
                  Object.keys(fontFamilies).map((fontFamily, fontIndex) => {
                    return (<option key={`font-${index}-${fontIndex}`} value={fontFamily}>{fontFamily}</option>);
                  })
                }
              </Select>
            </FormControl>
            <FormControl style={{width: '100%'}}>
              <Select
                id={`size-${index}`}
                onChange={e => changeCustomFontsProperty('size', index, e.target.value)}
                value={size}
              >
                {
                  FONT_SIZES.map((currentSize, sizeIndex) => {
                    return (<option key={`size-${index}-${sizeIndex}`} value={currentSize}>{currentSize}</option>);
                  })
                }
              </Select>
            </FormControl>
            <FormControl style={{width: '100%'}}>
              <ColorPicker
                animation="slide-up"
                alpha={alpha}
                color={color}
                id={`color-${index}`}
                onChange={e => changeCustomFontsProperty('color', index, hex2rgba(e.color, e.alpha))} />
            </FormControl>
          </LabelInput>
        );
      }));
    }
    const addNewLabelIndex = customFonts.labels.length;
    elements.push((
      <LabelInput
        handleOnChange={addCustomFontsLabel}
        index={addNewLabelIndex}
        label="Type an existing label name" 
        name="addNewLabel"
        key={`row-${addNewLabelIndex}`}
        sizes={[5,5]}
        value=""
      />
    ));

    return elements;
  }

  render() {
    if (!this.props.customFonts) {
      return 'Loading';
    }
    return this.renderLabelInputs(this.props);
  }
}

CustomFontsForm.propTypes = {
  customFonts: PropTypes.object
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addCustomFontsLabel,
      changeCustomFontsProperty,
      removeCustomFontsLabel,
    },
    dispatch
  );
};

const mapStateToProps = ({ options: { customFonts } }) => {
  return {
    customFonts,
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(CustomFontsForm);
