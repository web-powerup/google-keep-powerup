'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  FormControl,
  Grid,
  IconButton,
  TextField
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
export default class LabelInput extends Component {
  renderChildren(children, sizes) {
    return children ? children.map( (child, index) => 
      <Grid item={true} key={`item-${index}`} xs={sizes[index]}>
        { child }
      </Grid>
    ) : null;
  }

  renderDeletionButton(handleOnClick, index) {
    return handleOnClick && (
      <Grid item={true} xs={1}>
        <IconButton 
          aria-label="Remove"
          color="primary"
          onClick={() => handleOnClick(index, this.text.value) }
          variant="fab" 
        >
          <DeleteIcon />
        </IconButton>
      </Grid>
    );
  }

  render() {
    const { children, disabled, focus, handleOnChange, handleOnDelete, handleOnFocus, index, label, sizes, value } = this.props;
    const size = children ? 4 : 7;
    return (
      <Grid
        alignItems="center"
        className="container"
        container={true}
        direction="row"
        onFocus={ handleOnFocus }
        spacing={2}
      >     
        <Grid item={true} xs={size}>
          <FormControl style={{width: '100%'}}>
            <TextField
              id={`label-${index}`}
              disabled={disabled}
              inputRef={el => {
                if (el) {
                  this.text = el;
                  if (focus) {
                    el.focus();
                  }
                }
              }}
              onChange={(e) => handleOnChange(index, e.target.value)}
              label={label}
              value={value}
            />
          </FormControl>
        </Grid>
        { this.renderChildren(children, sizes, index) }
        { !disabled && this.renderDeletionButton(handleOnDelete, index) }
      </Grid>
    );
  }
}

LabelInput.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  disabled: PropTypes.bool,
  focus: PropTypes.bool,
  handleOnChange: PropTypes.func.isRequired,
  handleOnDelete: PropTypes.func,
  handleOnFocus: PropTypes.func,
  index: PropTypes.number.isRequired,
  label: PropTypes.string,
  sizes: PropTypes.array.isRequired,
  value: PropTypes.string.isRequired,
};
