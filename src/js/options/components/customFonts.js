'use es6';

import React from 'react';
import CustomFontsForm from './customFontsForm';
import Section from './section';

export default function render() {
  return (
    <Section title="Custom fonts" description="To give a personal touch to yours notes or make some of them even more prominent, type the label name and select the font family, size and color that you want to use for such notes.">
      <CustomFontsForm />
    </Section>
  );
}