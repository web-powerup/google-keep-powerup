'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Switch,
} from '@material-ui/core';

import LabelInput from './inputs/labelInput';
import { addMarkdownLabel, changeMarkdownLabel, removeMarkdownLabel, toggleMarkdownApplyAll} from '../actions';

class MarkdownForm extends Component {

  renderLabelInputs({ addMarkdownLabel, changeMarkdownLabel, markdown, markdownApplyAll, removeMarkdownLabel }) {
    const elements = markdown.map((label, index) => {
      const name = `markdown.${index}`;
      return (
        <LabelInput
          disabled={markdownApplyAll}
          handleOnChange={changeMarkdownLabel}
          handleOnDelete={removeMarkdownLabel}
          index={index}
          name={name}
          key={name}
          sizes={[5,5]}
          value={label}
        />
      );
    });
    !markdownApplyAll && elements.push((
      <LabelInput
        disabled={markdownApplyAll}
        handleOnChange={addMarkdownLabel}
        index={elements.length}
        label="Type an existing label name" 
        name="addNewLabel"
        key={`markdown.${elements.length}`}
        sizes={[5,5]}
        value=""
      />
    ));
    return elements;
  }

  render() {
    if (!this.props.markdown) {
      return 'Loading';
    }
    return (
      <form className="markdown">
        <FormControl style={{width: '100%'}} component="fieldset">
          <FormControlLabel
            control={<Switch checked={this.props.markdownApplyAll} onChange={(event,  checked) => this.props.toggleMarkdownApplyAll(checked)} name="all_notes" />}
            label="Apply markdown as default behavior to all notes  ( this option might slow down things )"
          />
        </FormControl>
        <Grid item={true} style={{marginTop: '3em'}} xs={4}>
          <FormLabel>Label</FormLabel>
        </Grid>
        {this.renderLabelInputs(this.props)}
      </form>
    );
  }
}

MarkdownForm.propTypes = {
  markdown: PropTypes.array,
  markdownApplyAll: PropTypes.bool,
  toggleMarkdownApplyAll: PropTypes.func
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addMarkdownLabel,
      changeMarkdownLabel,
      removeMarkdownLabel,
      toggleMarkdownApplyAll,
    },
    dispatch
  );
};

const mapStateToProps = ({ options: { markdown, markdownApplyAll} }) => {
  return {
    markdown,
    markdownApplyAll
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(MarkdownForm);
