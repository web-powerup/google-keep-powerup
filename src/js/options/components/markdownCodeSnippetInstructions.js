'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import hljs from 'highlight.js';
import 'highlight.js/styles/github.css';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Dialog,
  DialogTitle,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography 
} from '@material-ui/core';
import languages from './supportedLanguages';

function SupportedLanguagesDialog(props) {
  const { languages, onClose, open } = props;

  const handleClose = () => {
    onClose();
  };
  return (
    <Dialog onClose={handleClose} aria-labelledby="supported-languages" open={open}>
      <DialogTitle id="supported-languages">Supported languages</DialogTitle>
      <Card raised={false} style={{ overflow: 'auto', height: '50vh' }}>
        <Table aria-label="Supported languages">
          <TableHead>
            <TableRow>
              <TableCell>Language</TableCell>
              <TableCell align="right">Classes</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {languages.map(row => (
              <TableRow key={row.language}>
                <TableCell align="right">{row.language}</TableCell>
                <TableCell align="right">{row.classes.join(', ')}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Card>
    </Dialog>
  );
}

SupportedLanguagesDialog.propTypes = {
  languages: PropTypes.array.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
};

class MarkdownCodeSnippetInstructions extends Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  componentDidMount() {
    hljs.highlightBlock(this.node);
  }

  handleClose() {
    this.setState({
      open: false
    });
  }

  handleOpen() {
    this.setState({
      open: true
    });
  }

  render() {
    const codeSnippet = `
class HelloWorld {
    public static void main( String []args ) {
        System.out.println( "Hello World!" );
    }
}
`;
    const { open } = this.state;
    const handleClose = this.handleClose.bind(this);
    const handleOpen = this.handleOpen.bind(this);

    return (
      <React.Fragment>
        <Card className="section" elevation={1} id="codeSnippets" square={true}>
          <CardHeader title={
            <Typography style={{ paddingRight: '0.75em' }} variant="h5">Markdown with Code Snippet Highlighting</Typography>
          } />
          <CardContent>
            <Typography >In markdown notes you can also include code snippets with syntax highlighting. For instance a code snippet like:</Typography>
            <Typography paragraph={true} style={{whiteSpace: 'pre-wrap'}} variant="caption">
              ```java
              {codeSnippet}
              ```
            </Typography>
            <Typography>It will be rendered as follow:</Typography>
            <pre ref={(node) => this.node = node}>
              <code className="java">{codeSnippet.trim()}</code>
            </pre>
          </CardContent>
          <CardActions>
            <Button size="small" color="primary" onClick={handleOpen}>
              Supported languages
            </Button>
          </CardActions>
        </Card>
        <SupportedLanguagesDialog languages={languages} open={open} onClose={handleClose} />
      </React.Fragment>
    );
  }
}

export default MarkdownCodeSnippetInstructions;