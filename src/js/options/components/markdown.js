'use es6';

import React from 'react';
import MarkdownForm from './markdownForm';
import Section from './section';

export default function render() {
  return (
    <Section title="Markdown" description={'Markdown is a lightweight markup language that enables notes to have some basic formatting, click here if you want to <a href="https://daringfireball.net/projects/markdown/syntax" target="_blank" rel="noopener noreferrer">learn more about Markdown syntax</a>. Type the labels that you want to use to mark such notes to be interpreded as markdown.'}>
      <MarkdownForm />
    </Section>
  );
}