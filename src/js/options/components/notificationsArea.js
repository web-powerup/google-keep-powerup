'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  IconButton,
  Slide,
  Snackbar
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import {
  Alert,
} from '@material-ui/lab';

import { removeNotification } from '../actions';

class NotificationsArea extends Component {
  renderNotifications({ removeNotification, notifications}) {
    return notifications.map(({ message, severity } , index) => {
      const handleClose = () => removeNotification(index);
      return (<Snackbar key={`snack-${index}`}
        autoHideDuration={5000}
        onClose={handleClose}
        open={true}
        TransitionComponent={Slide}
      >
        <Alert 
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
          variant="filled" severity={severity}>
          {message}
        </Alert>
      </Snackbar>);
    });
  }

  render() {
    if (!this.props.notifications) {
      return null;
    }
    return this.renderNotifications(this.props);
  }
}

NotificationsArea.propTypes = {
  hideNotification: PropTypes.func,
  notifications: PropTypes.array
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      removeNotification,
    },
    dispatch
  );
};

const mapStateToProps = ({ notifications }) => {
  return {
    notifications,
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(NotificationsArea);
