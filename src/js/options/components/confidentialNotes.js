'use es6';

import React from 'react';
import ConfidentialNotesForm from './confidentialNotesForm';
import Section from './section';

export default function render() {
  return (
    <Section title="Confidential notes" description="Keep the content of your notes confidential without renouncing to have your Google Keep&trade; application open full screen. Type the labels that you want to use to mark such notes which content must be collapsed. The labels themselves and the title will be still visible but in order to see the content of such notes you will need to click and open the note.">
      <ConfidentialNotesForm />
    </Section>
  );
}