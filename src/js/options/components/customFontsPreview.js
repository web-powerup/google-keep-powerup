'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import OpenInNew from '@material-ui/icons/OpenInNew';
import FontManager from '../../features/FontManager';

class CustomFontsPreview extends Component {
  constructor() {
    super();
    this.fontManager = new FontManager();
  }

  getSelectedColor({ colors, focusedIndex }) {
    return colors[focusedIndex];
  }

  getSelectedFontFamilyName({ fonts, focusedIndex }) {
    return fonts[focusedIndex];
  }

  getSelectedFontSize({ sizes, focusedIndex }) {
    return sizes[focusedIndex];
  }

  renderPreviewParagraph(selectedFontColor, selectedFontFamilyName, selectedFontSize) {
    if (!selectedFontFamilyName) {
      return null;
    }
    const lineHeight = `${selectedFontSize + 0.125} rem`;
    return (
      <Typography paragraph={true} style={{ color: selectedFontColor, fontFamily: selectedFontFamilyName, fontSize: selectedFontSize + 'rem', lineHeight, minHeight: '60px', paddingTop: '15px' }} variant="body1">
        This is a preview of {selectedFontFamilyName} font family. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      </Typography>
    );
  }

  renderPreviewActions(selectedFontFamilyName) {
    if (!selectedFontFamilyName) {
      return null;
    }
    const url = `https://fonts.google.com/specimen/${selectedFontFamilyName}`;
    return (
      <Button href={url} rel="noreferrer noopener" size="small" target="_blank">Open font specs<OpenInNew /></Button>
    );
  }
  
  render() {
    const { customFonts } = this.props;
    const selectedFontColor = this.getSelectedColor(customFonts);
    const selectedFontFamilyName = this.getSelectedFontFamilyName(customFonts);
    const selectedFontSize = this.getSelectedFontSize(customFonts);
    if (selectedFontFamilyName) {
      this.fontManager.importFontFamily(selectedFontFamilyName);
    }
    return (
      <Card className="section customFonts preview" elevation={1} square={true}>
        <CardContent>
          { this.renderPreviewParagraph(selectedFontColor, selectedFontFamilyName, selectedFontSize) }
        </CardContent>
        <CardActions>
          { this.renderPreviewActions(selectedFontFamilyName) }
        </CardActions>
      </Card>
    );
  }
}

CustomFontsPreview.propTypes = {
  customFonts: PropTypes.object
};

const mapStateToProps = ({ options: { customFonts } }) => {
  return {
    customFonts,
  };
};

export default connect(mapStateToProps)(CustomFontsPreview);
