'use es6';

import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, CardHeader, Typography } from '@material-ui/core';

export default function render({children, id, description, title}) {
  return (
    <Card>
      <CardHeader id={id} title={title} />
      <CardContent>
        <Typography paragraph={true} variant="body1" dangerouslySetInnerHTML={{__html: description}} />
        {children}
      </CardContent>
    </Card>
  );
}

render.propTypes = {
  children: PropTypes.node.isRequired,
  description: PropTypes.string.isRequired,
  id: PropTypes.string,
  title: PropTypes.string.isRequired,
};