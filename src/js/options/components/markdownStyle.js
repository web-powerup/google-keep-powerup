'use es6';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  FormControl,
  FormHelperText,
  TextField,
} from '@material-ui/core';
import Section from './section';
import { changeMarkdownStyle } from '../actions';

class MarkdownStyle extends Component {

  render() {
    const { changeMarkdownStyle, error, value } = this.props;
    if (typeof value === "undefined") {
      return 'Loading';
    }
    return (
      <Section id="markdownStyle"
        description={'Feel free to customize the style of your markdown, you can get some inspiration from <a href="https://markdowncss.github.io">https://markdowncss.github.io/</a>. Write a CSS stylesheet in the following box:'}
        title="Markdown stylesheet"
      >
        <FormControl error={!!error} style={{width: '100%'}}>
          <TextField
            id="markdown-style"
            multiline
            rows={6}
            value={value}
            onChange={e => changeMarkdownStyle(e.target.value)}
            variant="outlined"
          />
          { error && <FormHelperText>{`${error.line}:${error.column} error: ${error.reason}`}</FormHelperText> }
        </FormControl>
      </Section>
    );
  }
}

MarkdownStyle.propTypes = {
  changeMarkdownStyle: PropTypes.func,
  error: PropTypes.object,
  value: PropTypes.string
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      changeMarkdownStyle
    },
    dispatch
  );
};

const mapStateToProps = ({ options: { markdownStyle: { value, error }}}) => {
  return {
    error,
    value
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(MarkdownStyle);
