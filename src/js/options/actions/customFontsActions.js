'use es6';

import ActionTypes from './actionTypes';

export function addCustomFontsLabel(index, value) {
  return {
    type: ActionTypes.CUSTOM_FONTS_ADD_LABEL,
    payload: {
      index,
      value
    }
  };
}

export function changeCustomFontsProperty(property, index, value) {
  let type = ActionTypes.CUSTOM_FONTS_CHANGE_LABEL;
  switch(property) {
  case 'color':
    type = ActionTypes.CUSTOM_FONTS_CHANGE_COLOR;
    break;
  case 'focus':
    type = ActionTypes.CUSTOM_FONTS_CHANGE_FOCUS;
    break;
  case 'font':
    type = ActionTypes.CUSTOM_FONTS_CHANGE_FONT;
    break;
  case 'label':
    type = ActionTypes.CUSTOM_FONTS_CHANGE_LABEL;
    break;
  case 'size':
    type = ActionTypes.CUSTOM_FONTS_CHANGE_SIZE;
    break;
  }
  return {
    type,
    payload: {
      index,
      value
    }
  };
}

export function removeCustomFontsLabel(index, value) {
  return {
    type: ActionTypes.CUSTOM_FONTS_REMOVE_LABEL,
    payload: {
      index,
      value
    }
  };
}