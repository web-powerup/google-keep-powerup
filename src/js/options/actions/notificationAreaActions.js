'use es6';

import ActionTypes from './actionTypes';

export function optionsSaved() {
  return {
    type: ActionTypes.OPTIONS_SAVED,
  };
}

export function addNotification(index, value) {
  return {
    type: ActionTypes.NOTIFICATIONAREA_ADD_NOTIFICATION,
    payload: {
      index,
      value
    }
  };
}

export function removeNotification(index) {
  return {
    type: ActionTypes.NOTIFICATIONAREA_REMOVE_NOTIFICATION,
    payload: {
      index
    }
  };
}