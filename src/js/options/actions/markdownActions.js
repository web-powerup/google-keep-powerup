'use es6';

import ActionTypes from './actionTypes';

export function addMarkdownLabel(index, value) {
  return {
    type: ActionTypes.MARKDOWN_ADD_LABEL,
    payload: {
      index,
      value
    }
  };
}

export function changeMarkdownLabel(index, value) {
  return {
    type: ActionTypes.MARKDOWN_CHANGE_LABEL,
    payload: {
      index,
      value
    }
  };
}

export function removeMarkdownLabel(index, value) {
  return {
    type: ActionTypes.MARKDOWN_REMOVE_LABEL,
    payload: {
      index,
      value
    }
  };
}


export function toggleMarkdownApplyAll(value) {
  return {
    type: ActionTypes.MARKDOWN_TOGGLE_APPLY_ALL,
    payload: {
      value
    }
  };
}