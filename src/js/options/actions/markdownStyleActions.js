'use es6';

import ActionTypes from './actionTypes';

export function changeMarkdownStyle(value) {
  return {
    type: ActionTypes.MARKDOWN_CHANGE_STYLE,
    payload: {
      value
    }
  };
}