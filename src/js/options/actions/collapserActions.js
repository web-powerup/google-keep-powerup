'use es6';

import ActionTypes from './actionTypes';

export function addCollapserLabel(index, value) {
  return {
    type: ActionTypes.COLLAPSER_ADD_LABEL,
    payload: {
      index,
      value
    }
  };
}

export function changeCollapserLabel(index, value) {
  return {
    type: ActionTypes.COLLAPSER_CHANGE_LABEL,
    payload: {
      index,
      value
    }
  };
}

export function removeCollapserLabel(index, value) {
  return {
    type: ActionTypes.COLLAPSER_REMOVE_LABEL,
    payload: {
      index,
      value
    }
  };
}