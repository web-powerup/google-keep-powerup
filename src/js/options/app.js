'use es6';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import { cyan, green, pink, red, yellow } from '@material-ui/core/colors';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { optionsSaved } from './actions';
import ApplicationContainer from './containers/application';
import OptionsApi from './api/optionsApi';
import reducers from './reducers';
import { debounce } from '../utils/common';

const api = new OptionsApi();
const OPTIONS_SAVED = optionsSaved();
const debouncedSetOptions = debounce(api.setOptions, 3000);

export default class Application {

  constructor() {
    this.theme = createMuiTheme({
      palette: {
        primary: cyan,
        secondary: pink,
        info: {
          main: cyan[600],
        },
        error: red,
        success: {
          main: green[600],
        },
        warning: {
          main: yellow[600],
        },
        contrastThreshold: 3,
        tonalOffset: 0.2,
      },
    });
  }
  
  handleStoreChange(store) {
    const state = store.getState();
    if (!state.saved && !state.notificationsChanged) {
      debouncedSetOptions(state.options).then(
        () => {
          store.dispatch(OPTIONS_SAVED);
        }
      );
    }
  }

  start() {
    api.getOptions()
      .then((options) => {
        const store = createStore(reducers,{ options });
        store.subscribe(() => { return this.handleStoreChange(store); });
        render(
          <MuiThemeProvider theme={this.theme}>
            <CssBaseline />
            <Provider store={store}>
              <ApplicationContainer version={options.version}/>
            </Provider>
          </MuiThemeProvider>,
          document.getElementById('app')
        );
      });
  }
}