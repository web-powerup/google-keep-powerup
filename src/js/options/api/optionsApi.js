'use es6';

import browser from 'webextension-polyfill';
import { DEFAULT_OPTIONS, LOCAL_STORAGE_KEY } from '../../constants/common.js';
import Logger from '../../utils/Logger';

let instance = null;
export default class OptionsApi {

  constructor() {
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  getFromLocalStorage() {
    return browser.storage && browser.storage.local.get()
    || new Promise(resolve => resolve(JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_KEY)))); //fallback for dev mode
  }
  
  getManifest() {
    return browser.runtime && browser.runtime.getManifest()
    || { version: '0.0.4.test'}; //fallback for web dev mode
  }
  
  setInLocalStorage(options) {
    return browser.storage && browser.storage.local.set(options)
    || new Promise(resolve => resolve(window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(options)))); //fallback for dev mode
  }

  migrate(options, version) {
    Logger.log('optionsApi|migrate|options', options);
    if (options && options.version !== version) {
      if (typeof options.labels === 'object') {
        options.confidentials = options.labels.slice(0);
        delete options.labels;
      }
      options.version = version;
      return instance.setInLocalStorage(options)
        .then(() => options);
    }
    return options;
  }

  getOptions() {
    const manifest = instance.getManifest();
    const version = manifest.version;
    const defaultOptions = DEFAULT_OPTIONS;
    return instance.getFromLocalStorage()
      .then(options => this.migrate(options, version))
      .then(options => {
        return Object.assign({}, defaultOptions, options);
      });
  }

  setOptions(options) {
    return instance.setInLocalStorage(options)
      .then(() => options);
  }
}
