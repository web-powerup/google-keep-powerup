'use es6';

import 'jsdom-global/register';
import { expect } from 'chai';
import sinon from 'sinon';
import tabKeyHandler from '../../src/js/features/tabKeyHandler';

describe('Tab Key Handler',  () => {
  let container;

  beforeEach(() => {
    document.body.innerHTML = '<div contenteditable="true" id="container">line1\nline2\nline3</div>';
    container = document.body.querySelector('#container');
    container.onkeydown = sinon.spy(tabKeyHandler);
    window.getSelection = () => {
      return {
        getRangeAt: () => {

        },
        isCollapsed: true,
        removeAllRanges: () => {}
      };
    };
    document.execCommand = () => {};
    
  });

  it('content shouldn\'t contain any tab', () => {
    const container = document.body.querySelector('#container');
    expect(container.textContent).to.eql('line1\nline2\nline3');
  });

  it('keydown event should trigger tabHandler', () => {
    const event = new KeyboardEvent('keydown', {
      keyCode: 9
    });
    container.dispatchEvent(event);
    expect(container.textContent).to.eql('line1\nline2\nline3');
    expect(container.onkeydown.calledOnce).to.be.true;
  });
});