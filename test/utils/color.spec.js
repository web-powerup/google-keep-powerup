'use es6';

import 'jsdom-global/register';
import { expect } from 'chai';
import { cssColor2ColorAlpha } from '../../src/js/utils/color';

describe('Color utils',  () => {
  it('when the color is formatted with uppercase hex notation', () => {
    const whiteUppercase = cssColor2ColorAlpha('#FFFFFF');
    expect(whiteUppercase.color).eql('#FFFFFF');
    expect(whiteUppercase.alpha).eql(100);
  });

  it('when the color is formatted with lower hex notation', () => {
    const whiteLowerCase = cssColor2ColorAlpha('#ffffff');
    expect(whiteLowerCase.color).eql('#FFFFFF');
    expect(whiteLowerCase.alpha).eql(100);
  });

  it('when the color is formatted with mixed case hex notation', () => {
    const whiteLowerCase = cssColor2ColorAlpha('#fF9900');
    expect(whiteLowerCase.color).eql('#FF9900');
    expect(whiteLowerCase.alpha).eql(100);
  });

  it('when the color is formatted with rgba notation', () => {
    const whiteLowerCase = cssColor2ColorAlpha('rgba(255,0,0,0.5)');
    expect(whiteLowerCase.color).eql('#FF0000');
    expect(whiteLowerCase.alpha).eql(50);
  });

  it('when the color is formatted with rgba notation with white spaces', () => {
    const whiteLowerCase = cssColor2ColorAlpha('rgba(255, 16, 126, 0.25)');
    expect(whiteLowerCase.color).eql('#FF107E');
    expect(whiteLowerCase.alpha).eql(25);
  });

  it('when the color is formatted with rgba notation', () => {
    const whiteLowerCase = cssColor2ColorAlpha('rgba(255,0,0,0.25)');
    expect(whiteLowerCase.color).eql('#FF0000');
    expect(whiteLowerCase.alpha).eql(25);
  });
});