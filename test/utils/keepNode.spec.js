'use es6';

//import 'jsdom-global/register';
import { compareByUpdatedTimestamp, compareByUserEditedTimestamp, isParentRoot } from '../../src/js/utils/keepNode';
import { expect } from 'chai';

const NODES = [{
    kind: 'notes#node',
    id: '1',
    parentId: 'root',
    timestamps: {kind: 'notes#timestamps', created: '2019-02-05T22:34:03.111Z', updated: '2019-07-21T11:35:00.235Z', trashed: '1970-01-01T00:00:00.000Z', userEdited: '2019-07-11T22:17:31.280Z'},
    title: '1'
  },{
    kind: 'notes#node',
    id: '2',
    parentId: 'root',
    timestamps: {kind: 'notes#timestamps', created: '2019-02-05T22:34:03.000Z', updated: '2019-07-21T12:36:02.235Z', trashed: '1970-01-01T00:00:00.000Z', userEdited: '2019-02-05T22:34:03.057Z'},
    title: '2'
  },{
    kind: 'notes#node',
    id: '3',
    parentId: '1',
    timestamps: {kind: 'notes#timestamps', created: '2019-02-05T22:34:03.031Z', updated: '2019-07-19T10:35:02.235Z', trashed: '1970-01-01T00:00:00.000Z', userEdited: '2019-07-23T10:25:27.791Z'},
    title: '3'
}];

describe('Keep Node utils',  () => {
  it('when the parent node is a root', () => {
    const rootNodes = NODES.filter(isParentRoot);
    expect(rootNodes.length).eql(2);
  });

  it('when comparing updated timestamps', () => {
    const sortedNodes = NODES.sort(compareByUpdatedTimestamp);
    expect(sortedNodes[0].title).eql('2');
    expect(sortedNodes[1].title).eql('1');
  });

  it('when comparing userEdited timestamps', () => {
    const sortedNodes = NODES.sort(compareByUserEditedTimestamp);
    expect(sortedNodes[0].title).eql('3');
    expect(sortedNodes[1].title).eql('1');
  });
});
