# Google Keep™ PowerUp [![CodeScene Code Health](https://codescene.io/projects/14210/status-badges/code-health)](https://codescene.io/projects/14210)

This project is not maintained anymore!

## Build zip bundle
After checking out the project install all dependencies:

```
npm install
```

Then build the source code and create a distribution zip archive as follow:

```
make dist
```

In the `dist` folder should have been created a new zip archive.