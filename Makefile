pwd := .
NODE_MODULES := node_modules
PACKAGE_VERSION := $(shell node -p -e "require('./public/manifest.json').version")


define colorecho
	$(if $(TERM),
		@tput setaf $2
		@echo $1
		@tput sgr0,
		@echo $1)
endef

default: build

help: info ## Show this help dialog
	@echo
	$(call colorecho, "⁉️ Help", 13)
	@echo
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' | column -c2 -t -s :)"

info: ## Show this info dialog
	@echo
	$(call colorecho, "----------------------------------------", 12)
	$(call colorecho, "Google Keep™ PowerUp - $(PACKAGE_VERSION)", 13)
	$(call colorecho, "----------------------------------------", 12)

clean: info ## Clean build directory
	@echo
	$(call colorecho, "Cleaning build and dist directories", 13)
	rm -rf build
	rm -rf dist

build: info clean eslint test build-mkdir install-dependencies copy-css copy-html copy-public build-js ## Build project

build-js: info ## Build js sources to build folder
	@echo
	$(call colorecho, "Building js sources for production", 13)
	npx webpack --env.mode production --mode production

development: info clean eslint test build-mkdir install-dependencies copy-css copy-html copy-public ## Build project for local
	@echo
	$(call colorecho, "Building sources for local usage", 13)
	npx webpack --env.mode development --mode development --progress --watch

build-mkdir: info ## Create build directory
	@echo
	$(call colorecho, "Creating build directory", 13)
	mkdir build
	mkdir build/css
	mkdir build/html
	mkdir build/js

dist: info clean eslint build dist-mkdir ## Create zip archive for Web store
	@echo
	$(call colorecho, "Zipping build directory", 13)
	(cd build && zip "../dist/google-keep-powerup$(PACKAGE_VERSION).zip" -r *)

dist-mkdir: info ## Create dist directory
	@echo
	$(call colorecho, "Creating dist directory", 13)
	mkdir dist

install-dependencies: info ## Install npm libraries to node_modules folder
	@echo
	$(call colorecho, "Install dependencies", 13)
	@if test ! -d $(NODE_MODULES); \
	then npm install; \
	else echo "...already installed."; \
	fi \

copy-css: info ## Copy css  to build/css folder
	@echo
	$(call colorecho, "Copying css", 13)
	cp -R src/css/* build/css

copy-html: info ## Copy html  to build/html folder
	@echo
	$(call colorecho, "Copying html", 13)
	cp -R src/html/* build/html


copy-public: info ## Copy public folder to build/ folder
	@echo
	$(call colorecho, "Copying public folder", 13)
	cp -R public/* build

eslint: info install-dependencies ## Run eslint against sources
	@echo
	$(call colorecho, "Eslint sources", 13)
	$(NODE_MODULES)/.bin/eslint --color src

eslint-fix: info install-dependencies ## Run eslint against sources with fix option
	@echo
	$(call colorecho, "Eslint sources", 13)
	$(NODE_MODULES)/.bin/eslint --color src --fix


server: info ## Build and run development server
	@echo
	$(call colorecho, "Running development server", 13)
	npx webpack-dev-server --open

test: info ## Build and run test
	@echo
	$(call colorecho, "Running tests", 13)
	npx mocha --require babel-core/register "./test/**/*.spec.js"
