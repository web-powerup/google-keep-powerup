const path = require('path');

module.exports = env => {
  return {
    devtool: env.mode === 'development' ? 'inline-source-map' : 'none',
    entry: {
      'background': './src/js/background.js',
      'content-apiProxyListener': './src/js/content-apiProxyListener.js',
      'content-main': './src/js/content-main.js',
      'content-loadData': './src/js/content-preloadData.js',
      'options': './src/js/options.js'
    },
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test:/\.css$/,
        use:['style-loader','css-loader']
      }]
    },
    node: {
      fs: 'empty',
    },
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'js/[name].js'
    },
    devServer: {
      contentBase: path.join(__dirname, 'build'),
      compress: true,
      port: 8080
    }
  }
};
